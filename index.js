/*db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "8765 4321",
        email: "janedoe@mail.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
});*/

// Advance Query Operators

// [SECTION] Comparison Query Operators
/*    
    $gt/$gte operator 
        - Allows us to find documents that have field number values greater than or equal to specified value.
        - Syntax:
            db.collectionName.find({field:{$gt/$gte: value}});
*/

db.users.find(
    {
    age:{
        $gt : 65 //greater than ( > )
    }
    }
)

db.users.find(
    {
    age:{
        $gte : 65 //greater than or equal to ( >= )
    }
    }
)

//----------------------

    // $lt / $lte operator (less than / less than or equal to)
        /*
                - Allows us to find documents that have the field number values less than or equal to a specified value.
                - Syntax:
                    db.collectionName.find({field: {$lt/$lte: value}});
             */

db.users.find(
    {
        age:{
            $lt: 65 //less than
        }
    }
);

db.users.find(
    {
        age:{
            $lte: 65 // less than or equal to
        }
    }
);

// $ne operator
/*
        - Allows us to find documents field number values not equal to a specified value.
        -Syntax:
            db.collectionName.find({field: { $ne: value}});
*/

db.users.find({
    age:{
        $ne : 82
    }
});

// $in operator
/*
        - Allows us to find documents with specific match criteria one with one field using different values.
        -  Selects the documents where the value of a field equals any value in the specified array. 
        - Syntax:
            db.collectionName.find({field: {$in: [valueA, valueB]}});
*/
db.users.find({
    lastName: {
        $in: ["Hawking", "Doe"] // must be in array form
    }
});

db.users.find({
    courses: {
        $in: ["HTML", "React"] //it's like 'OR' - will retrieve data that will meet at least one condition / criteria
    }
});

// [SECTION] Logical Query Operators

    // $or operator
    /*
        - Allows us to find documents that match a single criteria from multiple provide search criteria
        - Syntax:
            db.collectionName.find({ $or: [{fieldA:valueA}, {fieldB:valueB}]});
    */


// Multiple Fields
db.users.find({
    $or : [
        {firstName : "Neil"},
        { age: 21}
    ]
});

db.users.find({
    $or : [
        {firstName : "Neil"},
        { age: {$gt: 25}}
    ]
});

    //$and operator
    /*
        - Allows us to find documents matching multiple criteria in a single field.
        - Syntax:
            db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: valueB}]});
    */

db.users.find({
    $and :[
            {age : {$ne : 82}},
            {age : {$ne : 76}}
        ]
});

db.users.find({
    $and :[
            {age : {$ne : 82}},
            {department: "operations"}
        ]
})

// [SECTION] Field Projection
// To help with the readability of the values returned, we can include/exclude fields from the response.

// Inclusion
/*
    - Allows us to include/add specific fields only when retrieving a document.
    - The value provided is 1 to denote that the field is being included in the retrieval.
    - Syntax:
        db.collectionName.find({criteria}, {field: 1});
*/

db.users.find(
    {
        firstName : "Jane"
    },
    {
        firstName : 1,
        lastName : 1,
        contact : 1
    }
);


// Exclusion
/*
    - Allows us to exclude/remove specific fields only when retrieving documents.
    - The value provided is 0 to denote that fields is being excluded.
    - Syntax:
        db.users.find({criteria}, {field: 0});
*/

db.users.find(
    {
        firstName : "Jane"
    },
    {
        contact : 0,
        department : 0
    }
);

db.users.find(
    {firstName: "Jane"},
    {
        firstName : 1,
        lastName: 1,
        contact : 1,
        department : 0
    }
);